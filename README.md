# [enqueue](https://phppackages.org/s/enqueue)/[async-command](https://phppackages.org/p/enqueue/async-command)

[**enqueue/async-command**](https://packagist.org/packages/enqueue/async-command)
[![rank](https://pages-proxy.gitlab.io/phppackages.org/enqueue/async-command/rank.svg)](http://phppackages.org/p/enqueue/async-command)
Symfony async command

(Unofficial demo and howto)

* [Symfony Async Commands](https://blog.forma-pro.com/symfony-async-commands-f05052e3b205)
